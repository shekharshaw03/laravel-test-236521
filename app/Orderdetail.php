<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orderdetail extends Model
{
    //

    public function products(){
        return $this->hasOne(Product::class,'productCode','productCode');
    }
}
